import itertools

# Problem Set 4A
# Name: <your name here>
# Collaborators:
# Time Spent: x:xx

def get_permutations(sequence):
    '''
    Enumerate all permutations of a given string

    sequence (string): an arbitrary string to permute. Assume that it is a
    non-empty string.  

    You MUST use recursion for this part. Non-recursive solutions will not be
    accepted.

    Returns: a list of all permutations of sequence

    Example:
    >>> get_permutations('abc')
    ['abc', 'acb', 'bac', 'bca', 'cab', 'cba']

    Note: depending on your implementation, you may return the permutations in
    a different order than what is listed here.
    '''

    if len(sequence) == 2:
        return [sequence[0]+sequence[1],sequence[1]+sequence[0]]
    else:
        my_list =  [[n + i for n in set(get_permutations(sequence.replace(i,'',1)))] for i in set(sequence)]
        answer = []
        for element in my_list:
            for one_el in element:
                answer.append(one_el)
        return answer 
        #return [[n + i for n in get_permutations(sequence.replace(i,''))] for i in sequence] 
        #for 'abc' returns ['bca', 'cba'], ['acb', 'cab'], ['abc', 'bac']] 

def test_me(sequence):
    my_list = get_permutations(sequence)
    their_list = [''.join(word) for word in set(itertools.permutations(sequence))]
    
    print(f'{sequence} - input string')
    print(f'{my_list} - my answer')
    print(f'{their_list} - correct answer ')


if __name__ == '__main__':

    test_me('aaa')
    test_me('pog')
    test_me('cog')
        


